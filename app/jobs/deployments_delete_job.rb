class DeploymentsDeleteJob < ActiveJob::Base
  queue_as :default

  def perform(deployment_id)
    deployment = Deployment.find(deployment_id)
    if deployment.running?
      begin
        kubclient = Kube.new
        kubclient.deleteDeployment deployment
      rescue => e
        puts "Ops! Job was failed! Error was : #{e}"
        deployment.fail!
      end
    else
      EventJob.create  :event_type => 'error', :message => "Ops! DeploymentUpdateJob: Deployment #{deployment.name} is not running state!"
    end
  end
end
