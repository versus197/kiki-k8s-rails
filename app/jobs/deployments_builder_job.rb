class DeploymentsBuilderJob < ActiveJob::Base
  queue_as :default

  def perform(deployment_id)
    deployment = Deployment.find(deployment_id)
    if deployment.may_build?
      deployment.build!
      cmd_prebuild = "mkdir -p tmp/project && cd  tmp/project && rm -rf " + deployment.id.to_s + " && git clone " + deployment.repository + " " + deployment.id.to_s
      cmd_build = "cd tmp/project/" + deployment.id.to_s + "  &&  git reset --hard " + deployment.commit.to_s
      #TODO: build docker image and push it  to registry
      if system(cmd_prebuild) && system(cmd_build)
        #TODO: add docker image name after build to deployment.image and save
        deployment.ready!
        puts "deployment name " + deployment.name.to_s + " is done"
      else
        deployment.fail!
        puts "deployment name " + deployment.name.to_s + " is fail"
      end
    else
      deployment.fail!
    end
  end

end
