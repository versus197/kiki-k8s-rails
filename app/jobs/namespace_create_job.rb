class NamespaceCreateJob < ActiveJob::Base
  queue_as :default

  def perform(namespace)
    begin
      kubclient = Kube.new
      kubclient.createNamespace namespace
    rescue => e
        puts "Ops! Job was failed! Error was : #{e}"
    end
  end

end
