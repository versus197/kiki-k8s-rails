class NamespaceDeleteJob < ActiveJob::Base
  queue_as :default

  def perform(namespace)
    namespaceExist = false
    kubclient = Kube.new
    namespaces = kubclient.getNamespaces
    puts namespaces.to_s
    namespaces.each do |ns|
      if ns == namespace
        namespaceExist = true
      end
    end
    if  namespaceExist
      begin
        unless namespace == 'default' or namespace == 'kube-system'
          kubclient.deleteNamespace  namespace
        else
          EventJob.create  :event_type => 'error', :message => "You can't delete namespace " + namespace
        end
      rescue => e
        puts "Ops! Job was failed! Error was : #{e}"
      end
    else
      EventJob.create  :event_type => 'error', :message => "NamespaceDeleteJob: Namespace " + namespace + " not found!"
    end
  end

end
