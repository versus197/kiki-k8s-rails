class CheckRunningServicesJob < ActiveJob::Base
  queue_as :default

  def perform(*args)
    kubclient = Kubeclient::Client.new 'http://192.168.88.100:8080/api/' , 'v1'
    services = Service.get_running_or_locking
    services.each { |service|
      begin
        kub_service = kubclient.get_service service.get_kubernetes_name, service.namespace
      rescue => ex
        if service.may_unlock?
          service.unlock
          service.need_lock=true
        end
        service.clusterIP=''
        service.ready
        service.save!
        ServicesCreateJob.perform_later(service.id)
      end
    }
  end

end
