class ServicesCreateJob < ActiveJob::Base
  queue_as :default

  def perform(service_id)
    service = Service.find(service_id)
    if service.ready?
      begin
          kubclient = Kube.new
          kubclient.createService service

          #TODO add HaProxy
          #AddServiceToHaproxyJob.perform_later
          #service.run!

      rescue => e
        puts "Ops! Job ServicesCreateJob with service #{service.name} was failed! Error was : #{e}"
      end
    else
      EventJob.create  :event_type => 'error', :message => "ServicesCreateJob: Service #{service.name} is not ready state!"
    end
  end
end
