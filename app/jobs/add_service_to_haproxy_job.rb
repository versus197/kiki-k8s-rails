class AddServiceToHaproxyJob < ActiveJob::Base
  queue_as :default

  #http://net-ssh.github.io/net-ssh/


  def perform(*args)
#TODO добавить результаты в Events
    @services =  Service.getExternal
    renderer = ERB.new(File.read("#{Rails.root}/lib/assets/haproxy/haproxy.erb"))
    File.write("#{Rails.root}/tmp/haproxy.cfg", renderer.result(binding))

    Net::SSH.start(Rails.configuration.kubernets_haproxy_host, 'vagrant', :keys => Rails.configuration.kubernets_ssh_key, :keys_only => true) do |ssh|
      #ssh.scp.download!('/vagrant/date.txt', "#{Rails.root}/tmp/date.txt")
      ssh.exec!("cp /etc/haproxy/haproxy.cfg /etc/haproxy/haproxy.cfg.bac")
      ssh.scp.upload!("#{Rails.root}/tmp/haproxy.cfg", "/etc/haproxy/haproxy.cfg")
      output = ""
      output = ssh.exec!("sudo service haproxy reload")
      puts output
      ssh.close
    end
  end
end
