class DeploymentsDeployJob < ActiveJob::Base
  queue_as :default

  def perform(deployment_id)
    deployment = Deployment.find(deployment_id)
    if deployment.may_run?
      begin
        kubclient = Kube.new
        kubclient.createDeployment deployment
      rescue => e
        deployment.fail!
        puts "Ops! Job was failed! Error was : #{e}"
      end
    end
  end
end
