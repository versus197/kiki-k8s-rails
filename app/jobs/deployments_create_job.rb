class DeploymentsCreateJob < ActiveJob::Base
  queue_as :default
  @deployment = nil

  def perform(deployment_id)
    @deployment = Deployment.find(deployment_id)
    if @deployment.creating?
      puts "Start create namespace"
      kubclient = Kube.new
      kubclient.createNamespace @deployment.namespace
      unless @deployment.image.blank?
        @deployment.ready! if @deployment.may_ready?
        puts "deployment " + @deployment.name + " is ready"
      end
    else
      @deployment.fail!
      puts "deployment " + @deployment.name + " is fail"
    end
  end


end
