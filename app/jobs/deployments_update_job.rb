class DeploymentsUpdateJob < ActiveJob::Base
  queue_as :default

  def perform(deployment_id)
    deployment = Deployment.find(deployment_id)
    if deployment.running?
      begin
        kubclient = Kube.new
        kubclient.updateDeployment deployment
      rescue => e
        puts "Ops! Job  DeploymentsUpdateJob  was failed! Error was : #{e}"
        deployment.fail!
      end
       else
      EventJob.create  :event_type => 'error', :message => "Ops! DeploymentUpdateJob: Deployment #{deployment.name} is not running state!"
    end
  end
end
