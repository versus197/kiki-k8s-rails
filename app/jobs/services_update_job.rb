class ServicesUpdateJob < ActiveJob::Base
  queue_as :default

  def perform(service_id)
    service = Service.find(service_id)
    if service.running?
      begin
        kubclient = Kube.new
        kubclient.updateService service
      rescue => e
        puts "Ops! Job ServicesCreateJob with service #{service.name} was failed! Error was : #{e}"
      end
    else
      EventJob.create  :event_type => 'error', :message => "Ops! ServicesUpdateJob: Service #{service.name} is not running state!"
    end

  end
end
