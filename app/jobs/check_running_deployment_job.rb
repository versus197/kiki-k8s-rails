class CheckRunningDeploymentJob

  include Sidekiq::Worker
  include Sidetiq::Schedulable

    recurrence do
      minutely(10)
    end

  def perform(*args)
    kubclient = Kube.new
    deployments = Deployment.get_running_or_locking
    deployments.each { |deploy|
      puts "check deployment #{deploy.name}"
      begin
        kub_deployment = kubclient.getDeployment deploy.kubernetes_name, deploy.namespace
        #puts "kub_deployment = #{kub_deployment}"
      rescue => ex
        EventJob.create  :event_type => 'error', :message => "Ops! CheckRunningDeploymentJob: Deployment #{deploy.name} is not running!  Error was : #{ex} "
        if deploy.may_ready?
          deploy.ready!
          DeploymentsDeployJob.perform_later(deploy.id)
        elsif deploy.may_unlock?
          deploy.unlock!
          deploy.need_lock=true
          deploy.save!
          deploy.ready!
          DeploymentsDeployJob.perform_later(deploy.id)
        end
      end
    }
    puts "end check deployments job"
  end
end
