class ServicesDeleteJob < ActiveJob::Base
  queue_as :default

  def perform(service_id)
    service = Service.find(service_id)
    if service.running?
      begin
        kubclient = Kube.new
        kubclient.deleteService service
      rescue => e
        puts "Ops! Job ServicesDestroyJob with service #{service.name} was failed! Error was : #{e}"
      end
    else
      puts "Ops! Job ServicesDestroyJob with service #{service.name} is not running!"
      EventJob.create  :event_type => 'error', :message => "ServicesCreateJob: Service #{service.name} is not running state!"
    end
  end

end
