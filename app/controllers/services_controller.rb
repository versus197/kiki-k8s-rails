class ServicesController < ApplicationController
  before_action :set_service, only: [:show, :edit, :update, :destroy]

  # GET /services
  # GET /services.json
  def index
    @services = Service.all
  end

  # GET /services/1
  # GET /services/1.json
  def show
  end

  # GET /services/new
  def new
    @service = Service.new
    4.times do @service.service_ports.build end
    @deployments = Deployment.get_ready_or_running
  end

  # GET /services/1/edit
  def edit
    4.times do @service.service_ports.build end
    @deployments = Deployment.get_ready_or_running
  end

  # POST /services
  # POST /services.json

  def create
    @service = Service.new(service_params)
    respond_to do |format|
      if @service.save
        # service.dependecies.create(deployment: deployment)
        format.html { redirect_to services_path, notice: 'Service was successfully created.' }
        format.json { render :show, status: :created, location: @service }
      else
        4.times do @service.service_ports.build end
        @deployments = Deployment.get_ready_or_running
        format.html { render :new }
        format.json { render json: @service.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /services/1
  # PATCH/PUT /services/1.json
  def update
    respond_to do |format|
      if @service.update(service_params)
        format.html { redirect_to @service, notice: 'Service was successfully updated.' }
        format.json { render :show, status: :ok, location: @service }
      else
        format.html { render :edit }
        format.json { render json: @service.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /services/1
  # DELETE /services/1.json
  def destroy
    @service.destroy
    respond_to do |format|
      format.html { redirect_to services_url, notice: 'Service was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_service
      @service = Service.friendly.find(params[:id])
    end



    # Never trust parameters from the scary internet, only allow the white list through.
    def service_params
      params.require(:service).permit(:name, :state, :selector, :namespace, :tos,
                                      :service_ports_attributes => [:id, :name, :service_port, :target_port, :protocol, :_destroy]
      )
    end
end
