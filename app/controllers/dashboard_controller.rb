class DashboardController < ApplicationController

  def index
    @deployments = Deployment.all
    @services = Service.all
    @activities = Event.all.order(created_at: :desc).limit(40)
    @pods_count = Kube.new.getAllPods.count
    @component_status = Kube.new.getKubclient.get_component_statuses
    @events = Kube.new.getKubclient.get_events
    @nodes = Kube.new.getKubclient.get_nodes
    @namespaces = Kube.new.getNamespaces
    @nodes.each do  |node|
		  node.status.conditions.each do |st| 
			  if  st.type == 'Ready' && st.status 
          @health = '<div class="count green"><center> OK </center></div>'
        else
          @health = '<div class="count red"><center> ERROR </center></div>'
        end 
      end 
    end
    
  end

end
