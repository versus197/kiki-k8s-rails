class NamespacesController < ApplicationController
  
  def index
    @namespaces = Kube.new.getNamespaces
  end
  
  def create
  end
  
  def destroy
    respond_to do |format|
      format.html { redirect_to namespaces_url, notice: 'Namespace was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  private
  def namespace_params
    params.require(:namespace).permit(:name)
  end
  
end