class DeploymentsController < ApplicationController
  before_action :set_deployment, only: [:show, :edit, :update, :destroy, :build]

  # GET /deployments
  # GET /deployments.json
  def index
    @deployments = Deployment.all

  end

  # GET /deployments/1
  # GET /deployments/1.json
  def show

  end

  # GET /deployments/new
  def new
    @deployment = Deployment.new
    4.times do @deployment.environment_variables.build end
    4.times do @deployment.labels.build end
  end

  def build
    if @deployment.may_build?
    DeploymentBuildJob.perform_later(@deployment.id)
    notice = 'Deployment added to build queue.'
    else
      notice = "Deployment can't build now"
    end

    respond_to do |format|
      format.html { redirect_to @deployment, notice: notice }
      format.json { render json: notice }
    end


  end

  # GET /deployments/1/edit
  def edit
    4.times do @deployment.environment_variables.build end
    4.times do @deployment.labels.build end
  end

  # POST /deployments
  # POST /deployments.json
  def create
    @deployment = Deployment.new(deployment_params)

    respond_to do |format|
      if @deployment.save
        DeploymentsCreateJob.perform_later(@deployment.id) if @deployment.creating?
        format.html { redirect_to @deployment, notice: 'Deployment was successfully created.' }
        format.json { render :show, status: :created, location: @deployment }
      else
        4.times do @deployment.environment_variables.build end
        4.times do @deployment.labels.build end
        format.html { render :new }
        format.json { render json: @deployment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /deployments/1
  # PATCH/PUT /deployments/1.json
  def update
    respond_to do |format|
      if @deployment.update(deployment_params)
        if @deployment.may_build?
          DeploymentsBuilderJob.perform_later(@deployment.id)
        end
        format.html { redirect_to @deployment, notice: 'Deployment was successfully updated.' }
        format.json { render :show, status: :ok, location: @deployment }
      else
        4.times do @deployment.environment_variables.build end
        4.times do @deployment.labels.build end
        format.html { render :edit }
        format.json { render json: @deployment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /deployments/1
  # DELETE /deployments/1.json
  def destroy
    @deployment.destroy
    respond_to do |format|
      format.html { redirect_to deployments_url, notice: 'Deployment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_deployment
      @deployment = Deployment.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def deployment_params
      params.require(:deployment).permit(:name, :image, :pods, :namespace, :version, :description,
                                         :state,:repository, :commit, :containerPort,
                                         :environment_variables_attributes => [:id, :env_key, :env_value,  :_destroy],
                                         :labels_attributes => [:id, :label_key, :label_value,  :_destroy]

      )
    end
end
