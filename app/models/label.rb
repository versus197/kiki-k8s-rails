class Label < ActiveRecord::Base
  belongs_to :deployment

  #TODO: value  без пробелов и подчеркиваний и запятых, слово или слово-слово

  validates :label_key,
            :presence => true
  validates :label_value,
            :presence => true

  def to_s
    label_key.to_s + " => " + label_value.to_s
  end
end
