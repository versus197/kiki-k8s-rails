class Dependency < ActiveRecord::Base
  belongs_to :service
  belongs_to :deployment
end
