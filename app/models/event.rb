class Event < ActiveRecord::Base

  EVENTS_OPTIONS = %w(error warning info debug)

  validates :event_type, :inclusion => {:in => EVENTS_OPTIONS}, :presence => true

end
