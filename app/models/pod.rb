class Pod
  include ActiveModel::Model
  attr_accessor :kubclient, :name, :namespace, :nodeName, :labels

  def initialize(name, namespace)
    @kubclient = Kube.new.getKubclient
    @name = name
    @namespace = namespace
  end
  
  def get
    pod = @kubclient.get_pod name, namespace
    @nodeName = pod.spec.nodeName
    @labels = pod.metadata.labels.to_hash
    #@labels = pod.metadata.labels.to_hash.each {|key, value| puts "#{key} is #{value}" }
    return pod
  end
  
  def getLogs
    logs = @kubclient.get_pod_log @name , @namespace
    logs.to_s.gsub(/\n/, '<br/>').html_safe
  end
  
  def to_s
    
  end

end
