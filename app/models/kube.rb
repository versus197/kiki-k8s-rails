class Kube
  include ActiveModel::Model

  def initialize
    @kubclient = Kubeclient::Client.new Rails.configuration.kubernets_uri, Rails.configuration.kubernets_api, ssl_options: Rails.configuration.kubernets_ssl_options
	  @kubclient_extensions = Kubeclient::Client.new Rails.configuration.kubernets_extensions_uri, Rails.configuration.kubernets_extensions_api, ssl_options: Rails.configuration.kubernets_ssl_options
    #09.08.2016 "extensions/v1beta1" eq 'batch/v1'
    #@kubclient_job_extensions = Kubeclient::Client.new Rails.configuration.kubernets_extensions_uri, Rails.configuration.kubernets_extensions_batch_api, ssl_options: Rails.configuration.kubernets_ssl_options
  end
  
  def getKubclient
    @kubclient
  end
  
  def getKubclientExtensions
    @kubclient_extensions
  end

  def getNamespaces
    ns = []
    namespaces = @kubclient.get_namespaces
    namespaces.each do |n|
      ns.push n[:metadata][:name] 
    end
    return ns
  end
  
  def getDeployment(name, namespace)
    @kubclient_extensions.get_deployment name, namespace
  end

  def createNamespace(namespace)
    namespaceExist = false
    namespaces = getNamespaces
    namespaces.each do |ns|
      if ns == namespace
        namespaceExist = true
      end
    end
    unless  namespaceExist
      begin
        ns = Kubeclient::Namespace.new
        ns.metadata = {}
        ns.metadata.name = namespace
        @kubclient.create_namespace ns
        EventJob.create  :event_type => 'info', :message => "Successfully created namespace " + namespace
      rescue => e
        puts "Ops! Job was failed! Error was : #{e}"
        EventJob.create  :event_type => 'error', :message =>  "Ops! Namespace "+ namespace +" can't created! Error was : #{e}"
      end
    else
      EventJob.create  :event_type => 'error', :message => "NamespaceCreateJob: Namespace " + namespace + "  is already defined!"
    end
  end

  def deleteNamespace(namespace)
    begin
      unless namespace == 'default' or namespace == 'kube-system'
          @kubclient.delete_namespace namespace
          EventJob.create  :event_type => 'info', :message => "Successfully deleted namespace " + namespace
        else
          EventJob.create  :event_type => 'error', :message => "You can't delete namespace " + namespace
        end
    rescue => e
      EventJob.create  :event_type => 'error', :message =>  "Ops! Namespace "+ namespace +" can't deleted! Error was : #{e}"
    end
  end

  def createService(service)
    begin
      @kubclient.create_service(Kubeclient::Service.new(service.to_kubernetes))
      srv = @kubclient.get_service service.get_kubernetes_name, service.namespace
      puts " srv.spec.type is  #{srv.spec.type}"
      service.clusterIP=srv[:spec].clusterIP
      service.service_ports.each do |port|
        srv.spec.ports.each do |pp|
          port.node_port = pp.nodePort if pp.name == port.name
          puts "port.node_port is #{port.node_port}"
        end
      end if srv.spec.type == "NodePort"
      service.save!
      service.run!
      EventJob.create  :event_type => 'info', :message => 'Successfully created service ' + service.name
      if service.need_lock?
        service.lock!
        EventJob.create  :event_type => 'info', :message => 'Successfully locked service ' + service.name
      end
    rescue => e
      ev = EventJob.create :event_type => 'error', :message => "Ops! Service #{service.name}  can't created ! Error was : #{e}"
      service.fail
      service.lasterror = ev.id
      service.save
    end
  end

  def deleteService(service)
    begin
      @kubclient.delete_service service.get_kubernetes_name, service.namespace
      service.ready
      service.clusterIP=''
      service.service_ports.each do |port|
          port.node_port = ''
      end if service.tos == 'NodePort'
      service.save!
      EventJob.create  :event_type => 'info', :message => "Successfully deleted service #{service.name}"
    rescue => e
      ev = EventJob.create  :event_type => 'error', :message =>  "Ops! Job ServicesDestroyJob with service #{service.name} was failed! Error was : #{e}"
      service.fail
      service.lasterror = ev.id
      service.save
    end
  end

  def updateService(service)
    if service.running?
      begin
        srv = Kubeclient::Service.new(service.to_kubernetes)
        kubservice = @kubclient.get_service service.get_kubernetes_name, service.namespace
        srv[:metadata].resourceVersion = kubservice[:metadata].resourceVersion
        @kubclient.update_service(srv)
        service.run!
        EventJob.create  :event_type => 'info', :message => "Successfully updated service #{service.name}"
      rescue => e
        ev = EventJob.create  :event_type => 'error', :message => "Ops! Job ServicesUpdateJob with service #{service.name} was failed! Error was : #{e}"
        service.fail
        service.lasterror = ev.id
        service.save
    end
    else
      EventJob.create  :event_type => 'error', :message => "Ops! DeploymentUpdateJob: Deployment #{service.name} is not running state!"
    end
  end

  def getAllPods
    @kubclient.get_pods
  end
  
  def getAllRS
    @kubclient_extensions.get_replica_sets
  end

  def createDeployment(deployment)
    if deployment.may_run?
      begin
        @kubclient_extensions.create_deployment( Kubeclient::Deployment.new(deployment.to_kubernetes))
        deployment.run!
        if deployment.need_lock?
          deployment.lock!
          else
        end
        EventJob.create  :event_type => 'info', :message => "Successfully create deployment #{deployment.name}"
      rescue => e
        ev = EventJob.create  :event_type => 'error', :message => "Ops! Job DeploymentCreateJob with deployment #{deployment.name} was failed! Error was : #{e}"
        deployment.lasterror = ev.id
        deployment.fail
        deployment.save
      end
    else
      EventJob.create  :event_type => 'error', :message => "Ops! DeploymentCreateJob: Deployment #{deployment.name} is not may run state!"
    end
  end

  def buildDeployment(deployment)
    #TODO: work with jobs
  end

  def deleteDeployment(deployment)
    # fist replicas set 0 to shutdown pods
    # second delete deployment
    # third remove replica set for deployment
    if deployment.running?
      begin
        depls = @kubclient_extensions.get_deployments :namespace => deployment.namespace
        depls.each do |dd|
          if dd[:metadata][:name] == deployment.kubernetes_name
            dd[:spec][:replicas]=0
            @kubclient_extensions.update_deployment(dd)
            break
          end
        end
        @kubclient_extensions.delete_deployment deployment.kubernetes_name, deployment.namespace
        rs = @kubclient_extensions.get_replica_sets :namespace => deployment.namespace
        rs.each do |replicaset|
          rs_app =replicaset[:metadata][:labels][:app]
          if  rs_app == deployment.kubernetes_name
            rs_name = replicaset[:metadata][:name]
            @kubclient_extensions.delete_replica_set rs_name, deployment.namespace
            break
          end
        end
        EventJob.create  :event_type => 'info', :message => "delete deployment #{deployment.kubernetes_name} in kubernetes and change  deployment #{deployment} to ready state"
        deployment.ready!
      rescue => e
        ev = EventJob.create  :event_type => 'error', :message => "Ops! Job DeploymentsDeleteJob with deployment #{deployment.name} was failed! Error was : #{e}"
        deployment.lasterror = ev.id
        deployment.fail
        deployment.save
      end
    else
      EventJob.create  :event_type => 'error', :message => "Ops! DeploymentUpdateJob: Deployment #{deployment.name} is not running state!"
    end
  end

  def updateDeployment(deployment)
      if deployment.running?
        begin
          @kubclient_extensions.update_deployment( Kubeclient::Deployment.new(deployment.to_kubernetes))
          deployment.run!
          EventJob.create  :event_type => 'info', :message => "Successfully updated deployment #{deployment.name}"
        rescue => e
           ev = EventJob.create  :event_type => 'error', :message => "Ops! Job DeploymentUpdateJob with deployment #{deployment.name} was failed! Error was : #{e}"
           deployment.lasterror = ev.id
           deployment.fail
           deployment.save
        end
      else
        EventJob.create  :event_type => 'error', :message => "Ops! DeploymentUpdateJob: Deployment #{deployment.name} is not running state!"
       end
    end

end