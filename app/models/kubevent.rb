class Kubevent
  include ActiveModel::Model
  
  attr_accessor :kubclient, :events
  
  def initialize
    @kubclient = Kube.new.getKubclient
  end
  
  def get
    @kubclient.get_events
  end
  
end