class ReplicaSet
  include ActiveModel::Model
  
  attr_accessor :kubclient, :name, :namespace, :rs
  
  def initialize(name, namespace)
    @kubclient = Kube.new.getKubclientExtensions
    @name = name
    @namespace = namespace
    @rs = @kubclient.get_replica_set @name, @namespace
  end
  
  
  def get
    @rs
  end
  
  def getLabels
    @rs.metadata.labels.to_hash
  end
  
  def getReplicas
    @rs.spec.replicas
  end
  
end