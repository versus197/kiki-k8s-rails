class ServicePort < ActiveRecord::Base
  belongs_to :service

  before_validation(on: [:create, :update]) do
      node_port = 0 if node_port.nil?
  end

  validates :name,
            :presence => true

  validates :target_port,
            :presence => true,
            :numericality => true

  validates :service_port,
            :presence => true,
            :numericality => true

  PROTOCOL_TYPES = %w(UDP TCP)

  validates :protocol,
            :presence => true,
            :inclusion => {:in => PROTOCOL_TYPES}

  def to_s
    name
  end


end
