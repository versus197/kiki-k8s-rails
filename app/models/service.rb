class Service < ActiveRecord::Base

  include AASM
  extend FriendlyId

  before_validation(on: [:create, :update]) do
    Cyrillizer.language = :russian
    self.name = name.to_lat if attribute_present?("name")
    self.namespace = 'default' if namespace.blank?
  end

  friendly_id :name, use: :slugged

  has_many :service_ports, dependent: :destroy
  has_many :dependencies
  has_many :deployments, through: :dependencies
  belongs_to :app,  class_name: "Deployment", foreign_key: "selector"

  accepts_nested_attributes_for :service_ports,
                                :allow_destroy => true,
                                reject_if: lambda {|attributes| attributes[:name].blank?}
  validates :name,
            :presence => true,
            length: {in:1..16}
   validates :selector,
            :presence => true,
            length: {in:1..62}
  validates :namespace,
            :presence => true

  enum state: {
        creating: "10",
        ready:    "20",
        running:  "30",
        locking:  "40",
        failing:  "99"
  }

  scope :get_running_or_locking, -> { where("state = ? or state =? ", 30, 40) }
  scope :getExternal, -> {where(:tos => 'External')}

  aasm :column => :state, :enum => true do
      state :creating, :initial => true
      state  :ready, :running, :locking, :failing

    event :ready do
      transitions :from => [:creating, :running, :failing], :to => :ready
    end

    event :run do
      transitions :from => [:ready, :running], :to => :running
    end

    event :lock do
      after do
        self.need_lock = true
        self.save!
        end
      transitions :from => [:running], :to => :locking
    end

    event :unlock do
      after do
        self.need_lock = false
        self.save!
        end
      transitions :from => [:locking], :to => :running
    end

    event :fail do
      transitions :from => [:creating, :running, :ready, :locking], :to => :failing
    end
  end

  def to_kubernetes
     srv = {
      "kind": "Service",
      "apiVersion": "v1",
      "metadata": {
          "name": get_kubernetes_name,
          "namespace": get_selector_namespace,
          "labels": {
              "app": get_kubernetes_name
          }
      },
      "spec": {
          "clusterIP": clusterIP,
          "type": (tos == 'Internal') ? "ClusterIP" : "NodePort"  ,
          "ports": getPorts,
          "selector": {
              "app": get_selector
          }
      }
    }
    return srv
  end

  def get_selector
    Deployment.find(self.selector.to_i).kubernetes_name
  end
  
  def get_selector_namespace
    Deployment.find(self.selector.to_i).namespace
  end
  

  def getPorts
    ports = []
    service_ports.each { |port|
      h = {}
      h[:name] = port.name
      h[:protocol] =  port.protocol
      h[:port] =  port.service_port
      h[:targetPort] =  port.target_port
      ports.push h
    }
    return ports
  end

  public
  def get_kubernetes_name
    'srv-'+slug.downcase
  end

end
