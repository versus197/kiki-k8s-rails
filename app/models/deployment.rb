class Deployment < ActiveRecord::Base

  include AASM
  extend FriendlyId

  after_create :create_kubernetes_name

  before_validation(on: [:create, :update]) do
    Cyrillizer.language = :russian
    self.name = name.to_lat if attribute_present?("name")
  end

  friendly_id :name, use: :slugged

  has_many :environment_variables, dependent: :destroy
  has_many :labels, dependent: :destroy
  has_many :dependencies
  has_many :services, through: :dependencies
  has_one :selector, class_name: "Service", foreign_key: 'selector'

  accepts_nested_attributes_for :labels,
                                :allow_destroy => true,
                                reject_if: lambda {|attributes| attributes[:label_key].blank? or attributes[:label_key]=="app"}


  accepts_nested_attributes_for :environment_variables,
                                :allow_destroy => true,
                                reject_if: lambda {|attributes| attributes[:env_key].blank?}

  validates :name,
            :presence => true,
            length: {in:1..25}

  validates :pods,
            :numericality => true

  validates :version,
            :presence => true,
            length: {in:1..25}

  validates :containerPort,
            :presence => true,
            :numericality => true


  scope :by_name, ->{ order(:name)}

  scope :only_creating, -> { where("state = ? ", 10) }
  scope :only_building , -> { where("state = ? ", 30) }
  scope :only_ready, -> { where("state = ? ", 40) }
  scope :only_running , -> { where("state = ? ", 50) }
  scope :only_locking , -> { where("state = ? ", 60) }
  scope :only_failing , -> { where("state = ? ", 99) }

  scope :get_ready_or_running, -> { where("state = ? or state =? ", 40, 50) }
  scope :get_running_or_locking, -> { where("state = ? or state =? ", 50, 60) }

    enum state: {
        creating: "10",
        building: "30",
        ready:    "40",
        running:  "50",
        locking:  "60",
        failing:  "99"
  }

  aasm :column => :state, :enum => true do

    state :creating, :initial => true
    state :running, :ready, :locking, :building, :failing

    event :build do
      transitions :from => [:create], :to => :building
    end
    event :ready do
      transitions :from => [:building, :running, :failing , :creating], :to => :ready
    end
    event :run do
      transitions :from => [:ready, :running], :to => :running
    end
    event :fail do
      transitions :from => [:creating, :building, :running, :ready], :to => :failing
    end
    event :lock do
      after do
      self.need_lock = true
      self.save!
      end
      transitions :from => :running, :to => :locking
    end
    event :unlock do
      after do
      self.need_lock = false
      self.save!
      end
      transitions :from => :locking, :to => :running
    end
  end

 def to_s
   name
 end

  def to_kubernetes
     deployment = {
      apiVersion: 'extensions/v1beta1',
      kind: 'Deployment',
      metadata: {
        name: kubernetes_name,
        namespace: namespace.downcase
      },
      spec: {
        replicas: pods,
        template: {
          metadata: {
            labels: {
                 app: kubernetes_name
            }
          },
          spec: {
            containers: [
              {
                name: kubernetes_name,
                image: image,
                ports: [ { containerPort: containerPort.to_int} ],
                env: getEnv
              }
            ]
          }
        }
      }
    }
    deployment[:spec][:template][:metadata][:labels] = getLabels
     deployment
  end

  def getLabels
    h = { :app => kubernetes_name }
    labels.each { |label| h[label.label_key] = label.label_value }
    return h
  end

  def getEnv
    arr = []
    environment_variables.each { |env|
      h = {}
      h[:name] = env.env_key
      h[:value] = env.env_value
      arr.push h
    }
    arr
  end

  protected
  def create_kubernetes_name
    self.kubernetes_name = "dpl-#{slug.downcase}-#{version.gsub(/[^a-zA-Z0-9\-]/, '').to_s.downcase}"
    self.save
  end

end
