class EnvironmentVariable < ActiveRecord::Base
  belongs_to :deployment


  validates :env_key,
      :presence => true
  validates :env_value,
      :presence => true

  def to_s
    env_key.to_s + " => " + env_value.to_s
  end
end
