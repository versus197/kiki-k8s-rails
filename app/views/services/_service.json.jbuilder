json.extract! service, :id, :name, :state, :selector, :slug, :namespace, :tos, :created_at, :updated_at
json.url service_url(service, format: :json)