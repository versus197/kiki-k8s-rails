json.array!(@deployments) do |deployment|
  json.extract! deployment, :id, :name, :image, :pods, :namespace, :version, :description, :state
  json.url deployment_url(deployment, format: :json)
end
