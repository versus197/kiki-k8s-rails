require 'test_helper'

class ServiceTest < ActiveSupport::TestCase

  test "should not save service without  name" do
      service = Service.new
      assert_not service.save
  end

  test "should not save service with  name" do
      service = Service.new
      service.name = 'Service'
      assert_not service.save
  end

  test "should save service with  name and selector but not namespace and namespace eq default" do
      service = Service.new
      service.name = 'Service'
      service.selector = 1
      service.save
      assert_equal service.namespace, 'default'
  end

  test "should  service state eq creating" do
      service = Service.new
      service.name = 'Service'
      service.selector = 1
      service.save
      assert_equal service.state, 'creating'
  end

  test "should  service ports ssl protocol eq TCP" do
      service = Service.new
      service.name = 'Service'
      service.selector = 1
      service.service_ports.build :name => 'web', :service_port => 80, :protocol=>'TCP', :target_port => '80'
      service.save
      service.service_ports.create :name => 'https', :service_port => 443, :protocol=>'TCP', :target_port => '443'
      assert_equal service.service_ports[1].protocol, 'TCP'
  end

  test "should save service with  name and selector and namespace" do
      service = Service.new
      service.name = 'Service'
      service.selector = 1
      service.namespace = 'default'
      assert service.save
  end
  test "should  service app name eq First" do
      service = Service.first
      assert_equal service.app.name, 'First'
  end

  test "should run service.ready! and  service state eq ready " do
      service = Service.first
      service.ready!
      assert_equal service.state, 'ready'
  end
  test "should run service.lock! and  service state  eq locking " do
      service = Service.first
      service.ready!
      service.run!
      service.lock!
      assert_equal service.state, 'locking'
  end
end
