require 'test_helper'

class DeploymentTest < ActiveSupport::TestCase

  test "should save deployment with name" do
      deployment = Deployment.new
      deployment.name = 'testname'
      deployment.containerPort = 80
      deployment.namespace = 'default'
      deployment.pods=1
      deployment.version='0.1a'
      assert deployment.save
  end

  test "should  deployment  state eq ready" do
      deployment = Deployment.first
      assert_equal deployment.state, 'ready'
  end

    test "should  deployment selector name eq FirstService " do
      deployment = Deployment.first
      assert_equal deployment.selector.name, 'FirstService'
  end

  test "should not save deployment without  name" do
      deployment = Deployment.new
      assert_not deployment.save
  end

  test "should not save deployment with only name" do
      deployment = Deployment.new
      deployment.name = 'testname'
      assert_not deployment.save
  end

  test "should not save deployment with name and containerPort" do
      deployment = Deployment.new
      deployment.name = 'testname'
      deployment.containerPort = 80
      assert_not deployment.save
  end

  test "should not save deployment with name, containerPort, namespace" do
      deployment = Deployment.new
      deployment.name = 'testname'
      deployment.containerPort = 80
      deployment.namespace = 'default'
      assert_not deployment.save
  end

  test "should not save deployment with pods and name, containerPort, namespace" do
      deployment = Deployment.new
      deployment.name = 'testname'
      deployment.namespace = 'default'
      deployment.pods=1
      deployment.containerPort = 80
      assert_not deployment.save
  end

  test "should save deployment with label " do
      deployment = Deployment.new
      deployment.name = 'testname'
      deployment.containerPort = 80
      deployment.namespace = 'default'
      deployment.pods=1
      deployment.version='0.1a'
      deployment.labels.build :label_key => 'key', :label_value => 'value'
      assert deployment.save
  end

  test "should save Deployment with  EnvironmentVariable " do
      deployment = Deployment.new
      deployment.name = 'testname'
      deployment.containerPort = 80
      deployment.namespace = 'default'
      deployment.pods=1
      deployment.version='0.1a'
      deployment.environment_variables.build :env_key => 'key', :env_value => 'value'
      assert deployment.save
  end

  test "should save Deployment and  Label_key eq key " do
      deployment = Deployment.new
      deployment.name = 'testname'
      deployment.containerPort = 80
      deployment.namespace = 'default'
      deployment.pods=1
      deployment.version='0.1a'
      deployment.save
      deployment.labels.create :label_key => 'key', :label_value => 'value'
      deployment.environment_variables.create :env_key => 'key', :env_value => 'value'
      assert_equal deployment.labels[0].label_key, 'key'
  end
  test "should save Deployment and  env value  eq value " do
      deployment = Deployment.new
      deployment.name = 'testname'
      deployment.containerPort = 80
      deployment.namespace = 'default'
      deployment.pods=1
      deployment.version='0.1a'
      deployment.save
      deployment.labels.create :label_key => 'key', :label_value => 'value'
      deployment.environment_variables.create :env_key => 'key', :env_value => '1234'
      assert_equal deployment.environment_variables[0].env_value, '1234'
  end

  test "should deployment state is locking" do
    deployment = Deployment.first
    deployment.run!
    deployment.lock!
    assert_equal deployment.state, "locking"
  end

end
