# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160819133516) do

  create_table "dependencies", force: :cascade do |t|
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "service_id"
    t.integer  "deployment_id"
  end

  create_table "deployments", force: :cascade do |t|
    t.string   "name"
    t.string   "image"
    t.integer  "pods"
    t.string   "namespace"
    t.string   "version"
    t.text     "description"
    t.string   "state"
    t.string   "slug"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "repository"
    t.string   "commit"
    t.integer  "containerPort"
    t.string   "kubernetes_name"
    t.boolean  "need_lock",       default: false
    t.integer  "lasterror",       default: 0
  end

  add_index "deployments", ["slug"], name: "index_deployments_on_slug", unique: true

  create_table "environment_variables", force: :cascade do |t|
    t.string   "env_key"
    t.string   "env_value"
    t.integer  "deployment_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "environment_variables", ["deployment_id"], name: "index_environment_variables_on_deployment_id"

  create_table "events", force: :cascade do |t|
    t.string   "type"
    t.string   "event_type"
    t.text     "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id"
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type"

  create_table "labels", force: :cascade do |t|
    t.string   "label_key"
    t.string   "label_value"
    t.integer  "deployment_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "labels", ["deployment_id"], name: "index_labels_on_deployment_id"

  create_table "service_ports", force: :cascade do |t|
    t.string   "name"
    t.string   "protocol"
    t.integer  "service_port"
    t.integer  "target_port"
    t.integer  "service_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "node_port"
  end

  add_index "service_ports", ["service_id"], name: "index_service_ports_on_service_id"

  create_table "services", force: :cascade do |t|
    t.string   "name"
    t.string   "state"
    t.integer  "selector"
    t.string   "slug"
    t.string   "namespace"
    t.string   "tos"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "clusterIP"
    t.boolean  "need_lock",  default: false
    t.integer  "lasterror",  default: 0
  end

  add_index "services", ["namespace"], name: "index_services_on_namespace"
  add_index "services", ["slug"], name: "index_services_on_slug", unique: true

end
