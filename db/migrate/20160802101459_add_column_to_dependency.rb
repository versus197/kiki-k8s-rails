class AddColumnToDependency < ActiveRecord::Migration
  def change
    add_column :dependencies, :service_id, :integer
    add_column :dependencies, :deployment_id, :integer
  end
end
