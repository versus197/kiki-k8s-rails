class ChangeDataTypeForSelectorToService < ActiveRecord::Migration
  def change
    change_table :services do |t|
  	t.change :selector, :integer
    end
  end
end
