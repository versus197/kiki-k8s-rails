class CreateLabels < ActiveRecord::Migration
  def change
    create_table :labels do |t|
      t.string :label_key
      t.string :label_value
      t.references :deployment, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
