class AddKubernetesNameToDeployment < ActiveRecord::Migration
  def change
    add_column :deployments, :kubernetes_name, :string
  end
end
