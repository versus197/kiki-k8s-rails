class CreateServicePorts < ActiveRecord::Migration
  def change
    create_table :service_ports do |t|
      t.string :name
      t.string :protocol
      t.integer :service_port
      t.integer :target_port
      t.references :service, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
