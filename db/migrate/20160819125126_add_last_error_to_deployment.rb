class AddLastErrorToDeployment < ActiveRecord::Migration
  def change
    add_column :deployments, :lasterror, :integer, :default=>'0'
  end
end
