class AddClusterIpToService < ActiveRecord::Migration
  def change
    add_column :services, :clusterIP, :string
  end
end
