class AddGitRepoToDeployment < ActiveRecord::Migration
  def change
    add_column :deployments, :repository, :string
    add_column :deployments, :commit, :string
  end
end
