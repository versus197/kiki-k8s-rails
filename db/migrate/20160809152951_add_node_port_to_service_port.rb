class AddNodePortToServicePort < ActiveRecord::Migration
  def change
    add_column :service_ports, :node_port, :integer
  end
end
