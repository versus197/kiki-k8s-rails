class AddNeedLockToService < ActiveRecord::Migration
  def change
    add_column :services, :need_lock, :boolean, default: false
  end
end
