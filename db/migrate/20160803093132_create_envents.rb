class CreateEnvents < ActiveRecord::Migration
  def change
    create_table :envents do |t|
      t.string :type
      t.string :event_type
      t.text :message

      t.timestamps null: false
    end
  end
end
