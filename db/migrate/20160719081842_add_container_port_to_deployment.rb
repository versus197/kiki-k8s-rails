class AddContainerPortToDeployment < ActiveRecord::Migration
  def change
    add_column :deployments, :containerPort, :integer
  end
end
