class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :name
      t.string :state
      t.string :selector
      t.string :slug
      t.string :namespace
      t.string :tos

      t.timestamps null: false
    end
    add_index :services, :slug, unique: true
    add_index :services, :namespace
  end
end
