class CreateEnvironmentVariables < ActiveRecord::Migration
  def change
    create_table :environment_variables do |t|
      t.string :env_key
      t.string :env_value
      t.references :deployment, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
