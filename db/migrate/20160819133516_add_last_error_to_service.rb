class AddLastErrorToService < ActiveRecord::Migration
  def change
    add_column :services, :lasterror, :integer, :default=>'0'
  end
end
