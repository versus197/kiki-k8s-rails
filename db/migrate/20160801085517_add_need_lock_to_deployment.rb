class AddNeedLockToDeployment < ActiveRecord::Migration
  def change
    add_column :deployments, :need_lock, :boolean, default: false
  end
end
