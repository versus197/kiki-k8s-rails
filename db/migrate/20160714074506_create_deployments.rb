class CreateDeployments < ActiveRecord::Migration
  def change
    create_table :deployments do |t|
      t.string :name
      t.string :image
      t.integer :pods
      t.string :namespace
      t.string :version
      t.text :description
      t.string :state
      t.string :slug

      t.timestamps null: false
    end
    add_index :deployments, :slug, unique: true
  end
end
