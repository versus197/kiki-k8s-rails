#!/bin/bash
  export MASTER_IP=192.168.88.100
  export K8S_VERSION=1.2.5
  export ETCD_VERSION=2.2.1
  export FLANNEL_VERSION=0.5.5
  export FLANNEL_IFACE=eth1
  export FLANNEL_IPMASQ=true
service docker start
sleep 10
docker stop $(docker  ps  -aq)
sleep 5
docker rm -f  $(docker  ps  -aq)
sleep 10
docker run \
    --volume=/:/rootfs:ro \
    --volume=/sys:/sys:ro \
    --volume=/var/lib/docker/:/var/lib/docker:rw \
    --volume=/var/lib/kubelet/:/var/lib/kubelet:rw \
    --volume=/var/run:/var/run:rw \
    --net=host \
    --privileged=true \
    --pid=host \
    -d \
    gcr.io/google_containers/hyperkube-amd64:v${K8S_VERSION} \
    /hyperkube kubelet \
        --allow-privileged=true \
        --api-servers=http://localhost:8080 \
        --v=2 \
        --address=0.0.0.0 \
        --enable-server \
        --hostname-override=127.0.0.1 \
        --config=/etc/kubernetes/manifests-multi \
        --containerized \
        --cluster-dns=10.0.0.10 \
        --cluster-domain=cluster.local
rm ./kubectl
wget http://storage.googleapis.com/kubernetes-release/release/v${K8S_VERSION}/bin/linux/amd64/kubectl
chmod 0755 kubectl
PATH=$PATH:`pwd`
sleep 20
kubectl get nodes
kubectl create namespace kube-system
kubectl create -f ./skydns.yaml
kubectl apply -f ./kubernetes-dashboard.yaml
echo "check DNS - nslookup kubernetes.default"
kubectl run curl --image=radial/busyboxplus:curl -i --tty

