#!/bin/bash
  export MASTER_IP=192.168.88.100
  export K8S_VERSION=1.2.5
  export ETCD_VERSION=2.2.1
  export FLANNEL_VERSION=0.5.5
  export FLANNEL_IFACE=eth1
  export FLANNEL_IPMASQ=true

echo "clean docker"
docker stop $(docker  ps  -aq)
sleep 5
docker rm -f  $(docker  ps  -aq)
sleep 5
echo "start docker-bootstrap"
sudo sh -c 'docker daemon -H unix:///var/run/docker-bootstrap.sock -p /var/run/docker-bootstrap.pid --iptables=false --ip-masq=false --bridge=none --graph=/var/lib/docker-bootstrap 2> /var/log/docker-bootstrap.log 1> /dev/null &'
sleep 10
echo "start etcd"
sudo docker -H unix:///var/run/docker-bootstrap.sock run -d  --net=host gcr.io/google_containers/etcd-amd64:${ETCD_VERSION} /usr/local/bin/etcd --listen-client-urls=http://127.0.0.1:4001,http://${MASTER_IP}:4001 --advertise-client-urls=http://${MASTER_IP}:4001  --data-dir=/var/etcd/data
sleep 10
echo "add config to  etcd"
sudo docker -H unix:///var/run/docker-bootstrap.sock run --net=host gcr.io/google_containers/etcd-amd64:${ETCD_VERSION} etcdctl set /coreos.com/network/config '{ "Network": "10.1.0.0/16" }'
sleep 5
echo "start flannel"
sudo docker -H unix:///var/run/docker-bootstrap.sock run -d --net=host --privileged -v /dev/net:/dev/net quay.io/coreos/flannel:${FLANNEL_VERSION} /opt/bin/flanneld --ip-masq=${FLANNEL_IPMASQ} --iface=${FLANNEL_IFACE}
sleep 5
docker -H unix:///var/run/docker-bootstrap.sock exec $(docker -H unix:///var/run/docker-bootstrap.sock ps|grep flannel|awk '{ print $1 }') cat /run/flannel/subnet.env
echo "change bip as FLANNEL_SUBNET in /etc/default/docker "
service docker stop 
