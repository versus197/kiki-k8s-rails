#!/bin/bash
  export MASTER_IP=192.168.88.100
  export K8S_VERSION=1.2.5
  export ETCD_VERSION=2.2.1
  export FLANNEL_VERSION=0.5.5
  export FLANNEL_IFACE=eth1
  export FLANNEL_IPMASQ=true
service docker start
sleep 5
echo "clean docker"
docker stop $(docker  ps  -aq)
sleep 5
docker rm -f  $(docker  ps  -aq)
sleep 5
echo "run kubelet"
sudo docker run \
    --volume=/:/rootfs:ro \
    --volume=/sys:/sys:ro \
    --volume=/dev:/dev \
    --volume=/var/lib/docker/:/var/lib/docker:rw \
    --volume=/var/lib/kubelet/:/var/lib/kubelet:rw \
    --volume=/var/run:/var/run:rw \
    --net=host \
    --privileged=true \
    --pid=host \
    -d \
    gcr.io/google_containers/hyperkube-amd64:v${K8S_VERSION} \
    /hyperkube kubelet \
        --allow-privileged=true \
        --api-servers=http://${MASTER_IP}:8080 \
        --v=2 \
        --address=0.0.0.0 \
        --enable-server \
        --containerized \
        --cluster-dns=10.0.0.10 \
        --cluster-domain=cluster.local
sleep 5
echo "run kube proxy"
sudo docker run -d \
    --net=host \
    --privileged \
    gcr.io/google_containers/hyperkube-amd64:v${K8S_VERSION} \
    /hyperkube proxy \
        --master=http://${MASTER_IP}:8080 \
        --v=2

