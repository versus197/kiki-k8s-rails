#!/bin/bash
  export MASTER_IP=192.168.88.100
  export K8S_VERSION=1.2.5
  export ETCD_VERSION=2.2.1
  export FLANNEL_VERSION=0.5.5
  export FLANNEL_IFACE=eth1
  export FLANNEL_IPMASQ=true
service docker stop
sudo sh -c 'docker daemon -H unix:///var/run/docker-bootstrap.sock -p /var/run/docker-bootstrap.pid --iptables=false --ip-masq=false --bridge=none --graph=/var/lib/docker-bootstrap 2> /var/log/docker-bootstrap.log 1> /dev/null &'
sleep 10
docker -H unix:///var/run/docker-bootstrap.sock stop  $(docker -H unix:///var/run/docker-bootstrap.sock  ps  -q) 
docker -H unix:///var/run/docker-bootstrap.sock rm $(docker -H unix:///var/run/docker-bootstrap.sock  ps  -aq)
sleep 5
docker -H unix:///var/run/docker-bootstrap.sock run -d     --net=host     --privileged     -v /dev/net:/dev/net     quay.io/coreos/flannel:${FLANNEL_VERSION}     /opt/bin/flanneld         --ip-masq=${FLANNEL_IPMASQ}         --etcd-endpoints=http://${MASTER_IP}:4001         --iface=${FLANNEL_IFACE}
sleep 5
docker -H unix:///var/run/docker-bootstrap.sock exec $(docker -H unix:///var/run/docker-bootstrap.sock ps|grep flannel|awk '{ print $1 }') cat /run/flannel/subnet.env
echo "change bip as FLANNEL_SUBNET in /etc/default/docker "

