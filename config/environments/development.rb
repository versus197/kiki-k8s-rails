Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  #config.kubernets_uri = 'http://192.168.88.100:8080/api/'
  config.kubernets_uri = 'https://192.168.99.100:8443/api/'
  config.kubernets_api = 'v1'
  #config.kubernets_extensions_uri = 'http://192.168.88.100:8080/apis'
  config.kubernets_extensions_uri = 'https://192.168.99.100:8443/apis'
  config.kubernets_extensions_api = 'extensions/v1beta1'
  config.kubernets_extensions_batch_api = 'batch/v1'

  config.kubernets_haproxy_host = '192.168.88.100'
  config.kubernets_ssh_key = "#{Rails.root}/lib/assets/haproxy/private.key"
  config.kubernets_ssl_options = {
      client_cert: OpenSSL::X509::Certificate.new(File.read('/Users/versus/.minikube/apiserver.crt')),
      client_key:  OpenSSL::PKey::RSA.new(File.read('/Users/versus/.minikube/apiserver.key')),
      ca_file: '/Users/versus/.minikube/ca.crt',
      verify_ssl:  OpenSSL::SSL::VERIFY_PEER
  }
  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = false

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  # Asset digests allow you to set far-future HTTP expiration dates on all assets,
  # yet still be able to expire them through the digest params.
  config.assets.digest = true

  # Adds additional error checking when serving assets at runtime.
  # Checks for improperly declared sprockets dependencies.
  # Raises helpful error messages.
  config.assets.raise_runtime_errors = true

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true
end
