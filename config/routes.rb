Rails.application.routes.draw do
  
  root 'dashboard#index'

  get 'lock' => 'dashboard#index'
  get 'logout' => 'dashboard#index'
  get 'setting' => 'dashboard#index'

  resources :services do
    resources :service_ports
  end

  resources :deployments do
    get :build
    resources :environment_variables
    resources :labels
  end
  
  resources :namespaces, only: [:index, :create, :destroy]


  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'
  require 'sidetiq/web'

  #https://github.com/endofunky/sidetiq
  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
